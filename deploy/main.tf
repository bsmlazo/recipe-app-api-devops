terraform {
  backend "s3" {
    bucket         = "recipe-app-api-bsanmartin-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 4.48.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# de esta forma no es necesario asignarle el nombre de la región. Tomará el valor de la región con la que estamos trabajando.
data "aws_region" "current" {

}